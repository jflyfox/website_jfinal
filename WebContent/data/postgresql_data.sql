delete from tb_column;
commit;
delete from sys_user;
commit;
delete from sys_dict_detail;
commit;
delete from sys_dict;
commit;

insert into sys_dict ( dict_name, dict_type, dict_remark)
values ( '状态类型', 'stateType', null);
insert into sys_dict ( dict_name, dict_type, dict_remark)
values ('任务类别', 'taskCategory', null);
commit;

insert into sys_dict_detail (dict_type, detail_name, detail_code, detail_sort, detail_type, detail_state, detail_content, detail_remark, create_time, create_id)
values ('stateType', '人员运输', null, '0', null, null, null, null, '2013-11-15 14:11:24', 21);
insert into sys_dict_detail (dict_type, detail_name, detail_code, detail_sort, detail_type, detail_state, detail_content, detail_remark, create_time, create_id)
values ( 'stateType', '物资铁运', null, '0', null, null, null, null, '2013-11-15 14:11:46', 21);
insert into sys_dict_detail (dict_type, detail_name, detail_code, detail_sort, detail_type, detail_state, detail_content, detail_remark, create_time, create_id)
values ( 'stateType', '装备陆运', null, '0', null, null, null, null, '2013-11-15 14:11:37', 21);
insert into sys_dict_detail (dict_type, detail_name, detail_code, detail_sort, detail_type, detail_state, detail_content, detail_remark, create_time, create_id)
values ('taskCategory', '处突', '2', '0', null, null, null, null, '2013-11-15 10:59:35', 21);
insert into sys_dict_detail (dict_type, detail_name, detail_code, detail_sort, detail_type, detail_state, detail_content, detail_remark, create_time, create_id)
values ('taskCategory', '维稳', '3', '0', null, null, null, null, '2013-11-15 10:59:47', 21);
commit;

insert into sys_user (username, password, realname, state, endtime, tel, address, create_id, create_time)
values ('admin', '123456', null, null, null, null, null, null, null);
insert into sys_user (username, password, realname, state, endtime, tel, address, create_id, create_time)
values ('test', '123456', null, null, null, null, null, null, null);
commit;
INSERT INTO tb_column( id, parent_id, title, level, sort)
    VALUES (1, 0, '栏目', 1, 1);
INSERT INTO tb_column( id, parent_id, title, level, sort)
    VALUES (2, 1, '产品', 2, 1);
INSERT INTO tb_column( id, parent_id, title, level, sort)
    VALUES (3, 2, '护肤品', 3, 1);
INSERT INTO tb_column( id, parent_id, title, level, sort)
    VALUES (4, 2, '家用电器', 3, 1);
INSERT INTO tb_column( id, parent_id, title, level, sort)
    VALUES (5, 2, '家具', 3, 1);
INSERT INTO tb_column( id, parent_id, title, level, sort)
    VALUES (6, 2, '装修设计', 3, 1);
INSERT INTO sys_dict( dict_id, dict_name, dict_type, dict_remark)
    VALUES (3, '栏目类型', 'columnType', '');

INSERT INTO sys_dict_detail( detail_id, dict_type, detail_name, detail_code, detail_sort,  detail_type, detail_state, detail_content, detail_remark, create_time, create_id)
    VALUES (11, 'columnType', '一级目录', '1', '1',  null, null, null, null, '2013-11-15 14:11:24', 21);
INSERT INTO sys_dict_detail( detail_id, dict_type, detail_name, detail_code, detail_sort,  detail_type, detail_state, detail_content, detail_remark, create_time, create_id)
    VALUES (12, 'columnType', '二级目录', '1', '1',  null, null, null, null, '2013-11-15 14:11:24', 21);
INSERT INTO sys_dict_detail( detail_id, dict_type, detail_name, detail_code, detail_sort,  detail_type, detail_state, detail_content, detail_remark, create_time, create_id)
    VALUES (13, 'columnType', '三级目录', '1', '1',  null, null, null, null, '2013-11-15 14:11:24', 21);
INSERT INTO sys_dict_detail( detail_id, dict_type, detail_name, detail_code, detail_sort,  detail_type, detail_state, detail_content, detail_remark, create_time, create_id)
    VALUES (19, 'columnType', '文章', '1', '1',  null, null, null, null, '2013-11-15 14:11:24', 21);


/****postgre序列最大值处理*****/
select setval('sys_user_userid_seq',(select max(userid)+1 from sys_user));
select setval('sys_dict_detail_detail_id_seq',(select max(detail_id)+1 from sys_dict_detail));
select setval('sys_dict_dict_id_seq',(select max(dict_id)+1 from sys_dict));
select setval('tb_column_id_seq',(select COALESCE(max(id),0)+1 from tb_column));