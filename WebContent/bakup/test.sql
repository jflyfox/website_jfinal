--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.2
-- Dumped by pg_dump version 9.3.2
-- Started on 2014-02-07 16:21:15

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 179 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1986 (class 0 OID 0)
-- Dependencies: 179
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 170 (class 1259 OID 71796)
-- Name: seq_flyfox; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE seq_flyfox
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_flyfox OWNER TO test;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 172 (class 1259 OID 71800)
-- Name: sys_dict; Type: TABLE; Schema: public; Owner: test; Tablespace: 
--

CREATE TABLE sys_dict (
    dict_id integer NOT NULL,
    dict_name character varying(256) NOT NULL,
    dict_type character varying(64) NOT NULL,
    dict_remark character varying(256)
);


ALTER TABLE public.sys_dict OWNER TO test;

--
-- TOC entry 1987 (class 0 OID 0)
-- Dependencies: 172
-- Name: TABLE sys_dict; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON TABLE sys_dict IS '数据字典表';


--
-- TOC entry 1988 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN sys_dict.dict_id; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict.dict_id IS '主键';


--
-- TOC entry 1989 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN sys_dict.dict_name; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict.dict_name IS '字典名称';


--
-- TOC entry 1990 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN sys_dict.dict_type; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict.dict_type IS '字典类型';


--
-- TOC entry 1991 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN sys_dict.dict_remark; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict.dict_remark IS '字典说明';


--
-- TOC entry 174 (class 1259 OID 71813)
-- Name: sys_dict_detail; Type: TABLE; Schema: public; Owner: test; Tablespace: 
--

CREATE TABLE sys_dict_detail (
    detail_id integer NOT NULL,
    dict_type character varying(64),
    detail_name character varying(256),
    detail_code character varying(32),
    detail_sort character varying(32),
    detail_type character varying(32),
    detail_state character varying(32),
    detail_content character varying(256),
    detail_remark character varying(256),
    create_time character varying(32),
    create_id integer
);


ALTER TABLE public.sys_dict_detail OWNER TO test;

--
-- TOC entry 1992 (class 0 OID 0)
-- Dependencies: 174
-- Name: TABLE sys_dict_detail; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON TABLE sys_dict_detail IS '数据字典明细表';


--
-- TOC entry 1993 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN sys_dict_detail.detail_id; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict_detail.detail_id IS '主键';


--
-- TOC entry 1994 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN sys_dict_detail.dict_type; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict_detail.dict_type IS '字典类型';


--
-- TOC entry 1995 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN sys_dict_detail.detail_name; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict_detail.detail_name IS '名称';


--
-- TOC entry 1996 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN sys_dict_detail.detail_code; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict_detail.detail_code IS '编号';


--
-- TOC entry 1997 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN sys_dict_detail.detail_sort; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict_detail.detail_sort IS '序号';


--
-- TOC entry 1998 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN sys_dict_detail.detail_type; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict_detail.detail_type IS '类型';


--
-- TOC entry 1999 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN sys_dict_detail.detail_state; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict_detail.detail_state IS '状态';


--
-- TOC entry 2000 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN sys_dict_detail.detail_content; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict_detail.detail_content IS '说明';


--
-- TOC entry 2001 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN sys_dict_detail.detail_remark; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict_detail.detail_remark IS '备注';


--
-- TOC entry 2002 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN sys_dict_detail.create_time; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict_detail.create_time IS '创建时间';


--
-- TOC entry 2003 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN sys_dict_detail.create_id; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_dict_detail.create_id IS '创建人';


--
-- TOC entry 173 (class 1259 OID 71811)
-- Name: sys_dict_detail_detail_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE sys_dict_detail_detail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sys_dict_detail_detail_id_seq OWNER TO test;

--
-- TOC entry 2004 (class 0 OID 0)
-- Dependencies: 173
-- Name: sys_dict_detail_detail_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE sys_dict_detail_detail_id_seq OWNED BY sys_dict_detail.detail_id;


--
-- TOC entry 171 (class 1259 OID 71798)
-- Name: sys_dict_dict_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE sys_dict_dict_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sys_dict_dict_id_seq OWNER TO test;

--
-- TOC entry 2005 (class 0 OID 0)
-- Dependencies: 171
-- Name: sys_dict_dict_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE sys_dict_dict_id_seq OWNED BY sys_dict.dict_id;


--
-- TOC entry 176 (class 1259 OID 71829)
-- Name: sys_user; Type: TABLE; Schema: public; Owner: test; Tablespace: 
--

CREATE TABLE sys_user (
    userid integer NOT NULL,
    username character varying(32) NOT NULL,
    password character varying(32) NOT NULL,
    realname character varying(32),
    state character varying(32),
    endtime character varying(32),
    tel character varying(32),
    address character varying(32),
    create_id integer DEFAULT 0,
    create_time character varying(32)
);


ALTER TABLE public.sys_user OWNER TO test;

--
-- TOC entry 2006 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN sys_user.userid; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_user.userid IS '主键';


--
-- TOC entry 2007 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN sys_user.username; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_user.username IS '用户名称';


--
-- TOC entry 2008 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN sys_user.password; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_user.password IS '用户密码';


--
-- TOC entry 2009 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN sys_user.realname; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_user.realname IS '真实姓名';


--
-- TOC entry 2010 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN sys_user.state; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_user.state IS '状态';


--
-- TOC entry 2011 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN sys_user.endtime; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_user.endtime IS '失效日期';


--
-- TOC entry 2012 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN sys_user.tel; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_user.tel IS '联系电话';


--
-- TOC entry 2013 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN sys_user.address; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_user.address IS '联系地址';


--
-- TOC entry 2014 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN sys_user.create_id; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_user.create_id IS '创建者';


--
-- TOC entry 2015 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN sys_user.create_time; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN sys_user.create_time IS '创建时间';


--
-- TOC entry 175 (class 1259 OID 71827)
-- Name: sys_user_userid_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE sys_user_userid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sys_user_userid_seq OWNER TO test;

--
-- TOC entry 2016 (class 0 OID 0)
-- Dependencies: 175
-- Name: sys_user_userid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE sys_user_userid_seq OWNED BY sys_user.userid;


--
-- TOC entry 178 (class 1259 OID 71838)
-- Name: tb_column; Type: TABLE; Schema: public; Owner: test; Tablespace: 
--

CREATE TABLE tb_column (
    id integer NOT NULL,
    parent_id integer DEFAULT 0,
    title text,
    content text,
    type integer,
    level integer,
    sort integer,
    publish_time character varying(64),
    publish_user character varying(64),
    update_time character varying(64),
    start_time character varying(64),
    end_time character varying(64),
    create_time character varying(64),
    create_id integer,
    image_url character varying(256)
);


ALTER TABLE public.tb_column OWNER TO test;

--
-- TOC entry 2017 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.id; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.id IS '主键';


--
-- TOC entry 2018 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.parent_id; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.parent_id IS '父ID';


--
-- TOC entry 2019 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.title; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.title IS '题目';


--
-- TOC entry 2020 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.content; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.content IS '内容';


--
-- TOC entry 2021 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.type; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.type IS '类型';


--
-- TOC entry 2022 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.level; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.level IS '级别';


--
-- TOC entry 2023 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.sort; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.sort IS '顺序';


--
-- TOC entry 2024 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.publish_time; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.publish_time IS '发布时间';


--
-- TOC entry 2025 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.publish_user; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.publish_user IS '发布人';


--
-- TOC entry 2026 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.update_time; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.update_time IS '更新时间';


--
-- TOC entry 2027 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.start_time; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.start_time IS '开始时间';


--
-- TOC entry 2028 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.end_time; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.end_time IS '结束时间';


--
-- TOC entry 2029 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.create_time; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.create_time IS '创建时间';


--
-- TOC entry 2030 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.create_id; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.create_id IS '创建人';


--
-- TOC entry 2031 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN tb_column.image_url; Type: COMMENT; Schema: public; Owner: test
--

COMMENT ON COLUMN tb_column.image_url IS '图片URL';


--
-- TOC entry 177 (class 1259 OID 71836)
-- Name: tb_column_id_seq; Type: SEQUENCE; Schema: public; Owner: test
--

CREATE SEQUENCE tb_column_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tb_column_id_seq OWNER TO test;

--
-- TOC entry 2032 (class 0 OID 0)
-- Dependencies: 177
-- Name: tb_column_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test
--

ALTER SEQUENCE tb_column_id_seq OWNED BY tb_column.id;


--
-- TOC entry 1846 (class 2604 OID 71803)
-- Name: dict_id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY sys_dict ALTER COLUMN dict_id SET DEFAULT nextval('sys_dict_dict_id_seq'::regclass);


--
-- TOC entry 1847 (class 2604 OID 71816)
-- Name: detail_id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY sys_dict_detail ALTER COLUMN detail_id SET DEFAULT nextval('sys_dict_detail_detail_id_seq'::regclass);


--
-- TOC entry 1848 (class 2604 OID 71832)
-- Name: userid; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY sys_user ALTER COLUMN userid SET DEFAULT nextval('sys_user_userid_seq'::regclass);


--
-- TOC entry 1850 (class 2604 OID 71841)
-- Name: id; Type: DEFAULT; Schema: public; Owner: test
--

ALTER TABLE ONLY tb_column ALTER COLUMN id SET DEFAULT nextval('tb_column_id_seq'::regclass);


--
-- TOC entry 2033 (class 0 OID 0)
-- Dependencies: 170
-- Name: seq_flyfox; Type: SEQUENCE SET; Schema: public; Owner: test
--

SELECT pg_catalog.setval('seq_flyfox', 1, false);


--
-- TOC entry 1972 (class 0 OID 71800)
-- Dependencies: 172
-- Data for Name: sys_dict; Type: TABLE DATA; Schema: public; Owner: test
--

COPY sys_dict (dict_id, dict_name, dict_type, dict_remark) FROM stdin;
1	状态类型	stateType	\N
2	任务类别	taskCategory	\N
3	栏目类型	columnType	
\.


--
-- TOC entry 1974 (class 0 OID 71813)
-- Dependencies: 174
-- Data for Name: sys_dict_detail; Type: TABLE DATA; Schema: public; Owner: test
--

COPY sys_dict_detail (detail_id, dict_type, detail_name, detail_code, detail_sort, detail_type, detail_state, detail_content, detail_remark, create_time, create_id) FROM stdin;
1	stateType	人员运输	\N	0	\N	\N	\N	\N	2013-11-15 14:11:24	21
2	stateType	物资铁运	\N	0	\N	\N	\N	\N	2013-11-15 14:11:46	21
3	stateType	装备陆运	\N	0	\N	\N	\N	\N	2013-11-15 14:11:37	21
4	taskCategory	处突	2	0	\N	\N	\N	\N	2013-11-15 10:59:35	21
5	taskCategory	维稳	3	0	\N	\N	\N	\N	2013-11-15 10:59:47	21
11	columnType	目录	1	1	\N	\N	\N	\N	2013-11-15 14:11:24	21
19	columnType	文章	2	2	\N	\N	\N	\N	2013-11-15 14:11:24	21
12	columnType	新文章	3	3	\N	\N	\N	\N	2013-11-15 14:11:24	21
\.


--
-- TOC entry 2034 (class 0 OID 0)
-- Dependencies: 173
-- Name: sys_dict_detail_detail_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test
--

SELECT pg_catalog.setval('sys_dict_detail_detail_id_seq', 20, true);


--
-- TOC entry 2035 (class 0 OID 0)
-- Dependencies: 171
-- Name: sys_dict_dict_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test
--

SELECT pg_catalog.setval('sys_dict_dict_id_seq', 4, true);


--
-- TOC entry 1976 (class 0 OID 71829)
-- Dependencies: 176
-- Data for Name: sys_user; Type: TABLE DATA; Schema: public; Owner: test
--

COPY sys_user (userid, username, password, realname, state, endtime, tel, address, create_id, create_time) FROM stdin;
1	admin	123456	\N	\N	\N	\N	\N	\N	\N
\.


--
-- TOC entry 2036 (class 0 OID 0)
-- Dependencies: 175
-- Name: sys_user_userid_seq; Type: SEQUENCE SET; Schema: public; Owner: test
--

SELECT pg_catalog.setval('sys_user_userid_seq', 2, true);


--
-- TOC entry 1978 (class 0 OID 71838)
-- Dependencies: 178
-- Data for Name: tb_column; Type: TABLE DATA; Schema: public; Owner: test
--

COPY tb_column (id, parent_id, title, content, type, level, sort, publish_time, publish_user, update_time, start_time, end_time, create_time, create_id, image_url) FROM stdin;
4	2	珠宝	<title>new document</title><meta name="generator" content="editplus"/><meta name="author" content=""/><meta name="keywords" content=""/><meta name="description" content=""/><pre>&nbsp;内核裁剪\r\n\t1.源码目录：/usr/src/kernels或/usr/src下\r\n\t2.内核裁剪目录介绍：http://blog.csdn.net/skyflying2012/article/details/8660677\r\n\t3.menuconfig配置：\r\n\thttp://wenku.baidu.com/view/efc745e8856a561252d36fe7.html\r\n\thttp://www.chinaunix.net/old_jh/4/1051808.html\r\n\t4.内核编译：\r\n\thttp://www.cnblogs.com/top5/archive/2011/03/04/1970645.html\r\n\t&nbsp;linux2.6内核编译步骤：http://blog.chinaunix.net/uid-26404477-id-3126818.html\r\n\t&nbsp;http://blog.csdn.net/csdn_document/article/details/416838</pre><pre>&nbsp;&nbsp;问题解决\r\n&nbsp;&nbsp;1.\r\n&nbsp;&nbsp;编译Linux2.6.31.7内核出错,找不到ncurses&nbsp;libraries&nbsp;，该怎么办\r\n\tUnable&nbsp;to&nbsp;find&nbsp;the&nbsp;ncurses&nbsp;libraries&nbsp;or&nbsp;the\r\n\t&nbsp;***&nbsp;required&nbsp;header&nbsp;files.\r\n\t&nbsp;***&nbsp;&#39;make&nbsp;menuconfig&#39;&nbsp;requires&nbsp;the&nbsp;ncurses&nbsp;libraries.\r\n&nbsp;&nbsp;解决：安转ncurses开发库&nbsp;如果是debian或ubuntu系统可用命令&nbsp;apt-get&nbsp;install&nbsp;libncurses5-dev\r\n&nbsp;&nbsp;(CentOS或RedHat用yum&nbsp;install&nbsp;ncurses\\yum&nbsp;install&nbsp;ncurses-devel&nbsp;\r\n&nbsp;&nbsp;参考：http://blog.sina.com.cn/s/blog_66927d490100qi5l.html)</pre><!--!doctype-->	19	3	2	2014-01-27	\N	\N	2014-01-27	2015-01-22	\N	\N	\N
1	0	栏目	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta charset="UTF-8"/><title>Chrome定制安装</title><link rel="stylesheet" href="./Chrome定制安装/style.css" type="text/css" media="screen"/><meta name="robots" content="index,follow"/><meta name="description" content="Chrome定制安装"/><ul id="postlist" class=" list-paddingleft-2"><li><h2 class="transparent-title">Chrome定制安装</h2><p><a href="http://brilliance.blog.com/2010/05/19/chrome%E5%AE%9A%E5%88%B6%E5%AE%89%E8%A3%85/">Chrome定制安装</a><br/>\r\n\t\t\t\t\t\t\t\t\t: “<span style="line-height: 1.8em">用过Chrome的童鞋肯定会发现,Chrome安装过程用户无法插手,XP下强制安装在用户目录下,(PS.Google的东西貌似都有这个习惯…)</span></p><p><span style="line-height: 1.8em">可是这种做法让某些用户十分不爽,也使某些用户十分不便.例如我的20G硬盘,C盘分了4G,Google的东西默认的强制占用我不多的系统盘资源….让我十分不爽.</span></p><p><span style="line-height: 1.8em">尤其是它浏览网页的时候产生的大量缓存文件都放在安装目录下的”User\r\n\t\t\t\t\t\t\t\t\tData”中,稍不注意我的系统托盘就提示空间不足= =!</span></p><p><span style="line-height: 1.8em">OK,废话不多说,接下来对强制安装不爽的童鞋们,我们来定制它.</span></p><p><span style="line-height: 1.8em"><span style="font-weight: bold">1.更改安装路径.</span></span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t1)首先默认安装Chrome(例如4.0.249.89),然后进入它的安装目录.</span></p><p><span style="line-height: 1.8em"><span style="color: #33ff00; line-height: 1.8em"> Win7/Vista下,\r\n\t\t\t\t\t\t\t\t\t\tWin+R运行 %UserProfile%\\AppData\\Local\\Google\\Chrome<span style="font-weight: bold"> XP下, Win+R运行\r\n\t\t\t\t\t\t\t\t\t\t\t%UserProfile%\\Local Settings\\Application Data\\Google\\Chrome</span></span></span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t2).进入Application文件夹.将Application\\4.0.249.89\\目录下所有的文件移动到Application\\目录下</span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t3).将Application\\文件夹改名,例如改成Chrome.然后移动到你想要安装Chrome的目录下,例如我的\r\n\t\t\t\t\t\t\t\t\tD:\\Program Files\\Chrome</span></p><p><span style="line-height: 1.8em"><span style="font-weight: bold">2.更改缓存文件路径.</span></span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t1).随便进入一个你想要放置缓存文件的目录下,例如我的 D:\\Program Files\\Chrome</span></p><p><span style="line-height: 1.8em"> 2).在此目录下新建一个文件夹 ,例如\r\n\t\t\t\t\t\t\t\t\tChrome_Temp(此目录将来存放缓存文件).</span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t3).如果你以前曾经用过Chrome了,并且有一些插件,账户信息,历史记录等等,可以直接将原安装目录下\\Google\\Chrome\\User\r\n\t\t\t\t\t\t\t\t\tData文件夹中的 所有文件都移动到新建的缓存文件目录下.</span></p><p><span style="line-height: 1.8em"><span style="font-weight: bold">3.初始化Chrome,使变更生效.最重要的一步了</span></span></p><p><span style="line-height: 1.8em"> 1).进入命令提示符, <span style="color: #66ff00; line-height: 1.8em">Win+R 运行 cmd</span></span></p><p><span style="line-height: 1.8em"> 2).切换到自定义的Chrome安装路径.\r\n\t\t\t\t\t\t\t\t\t例如 <span style="color: #66ff00; line-height: 1.8em">cd\r\n\t\t\t\t\t\t\t\t\t\tD:\\Program Files\\Chrome</span></span></p><p><span style="line-height: 1.8em"> 3).运行 <span style="color: #66ff00; line-height: 1.8em">chrome.exe\r\n\t\t\t\t\t\t\t\t\t\t-first-run -user-data-dir=</span><span style="color: #AAAA00; line-height: 1.8em">D:\\Program\r\n\t\t\t\t\t\t\t\t\t\tFiles\\Chrome\\Chrome_Temp (黄色部分是你自己的缓存文件位置)</span></span></p><p><span style="line-height: 1.8em"> 弹出安装窗口,下一步,下一步…..</span></p><p><span style="line-height: 1.8em"><span style="font-weight: bold">4.创建快捷方式,以运行用户自定义的Chrome.(必需)</span></span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t1).在chrome.exe上右键单击–&gt;创建快捷方式,</span></p><p><span style="line-height: 1.8em"> 或者\r\n\t\t\t\t\t\t\t\t\t右键单击–&gt;发送到–&gt;桌面快捷方式</span></p><p><span style="line-height: 1.8em"> (或者 在任意空白地方\r\n\t\t\t\t\t\t\t\t\t右键—&gt;新建快捷方式–&gt;选择chrome.exe)</span></p><p><span style="line-height: 1.8em"> 然后这个快捷方式可以随便复制或者移动了.</span></p><p><span style="line-height: 1.8em"> 2).修改快捷方式的参数</span></p><p><span style="line-height: 1.8em"> 在快捷方式上 右键—&gt;属性\r\n\t\t\t\t\t\t\t\t\t在目标栏最后添加参数 -<span style="color: #66ff33; line-height: 1.8em">user-data-dir=</span><span style="color: #B1B100; line-height: 1.8em">D:\\Program\r\n\t\t\t\t\t\t\t\t\t\tFiles\\Chrome_temp</span></span></p><p><span style="line-height: 1.8em"> 例如我的 <span style="color: #AAAA00; line-height: 1.8em">“D:\\Program\r\n\t\t\t\t\t\t\t\t\t\tFiles\\Chrome\\chrome.exe” </span><span style="color: #66ff00; line-height: 1.8em">-user-data-dir=D:\\Program\r\n\t\t\t\t\t\t\t\t\t\tFiles\\Chrome_temp</span></span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t如此一来运行这个快捷方式,浏览网页产生的缓存文件便会保存在自定义的目录下了.</span></p><p><span style="line-height: 1.8em"><span style="font-weight: bold">5.修改默认文件的打开方式.</span>(非必需,但是不修改的话可能会发生你不想看到的结果=\r\n\t\t\t\t\t\t\t\t\t=!)</span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t1).打开”我的电脑”,按Alt,工具—&gt;文件夹选项—&gt;文件类型</span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t2).如果你曾默认安装过Chrome,会看到某些类型的文件默认是用Chrome打开的,例如HTML,HTM等等.</span></p><p><span style="line-height: 1.8em"> 找出这些文件,然后选中\r\n\t\t\t\t\t\t\t\t\t单击”高级”,在弹出的窗口中的操作框里找到”open”,选中 单击”编辑”.</span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t会看到”用于执行的应用程序”这一栏,将里边的链接添加 相关参数.</span></p><p><span style="line-height: 1.8em"> 例如我的 <span style="color: #B2B309; line-height: 1.8em">“D:\\Program\r\n\t\t\t\t\t\t\t\t\t\tFiles\\Chrome\\chrome.exe” -user-data-dir=Chrome_temp\r\n\t\t\t\t\t\t\t\t\t\t-bookmark-menu — “%1″</span></span></p><p><span style="font-size: 18px; line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\tPS.至于都有哪些类型….自己找找吧.就那么几个.</span></p><p><span style="line-height: 1.8em">经过了这么几步之后,你的Chrome已经自定义安装了.</span></p><p>只是这样做有一个后遗症……无法自动更新了….不过你可以手动更新,或者利用类似360那样的软件升级管理工具来进行升级.</p><p>—————————————————————————————————————————</p><p>Chrome的定制可不仅仅这么一个.通过修改快捷方式的参数,还可以实现更多的定制.</p><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\r\n\t\t\t\t\t\t\t\t1、将标签栏缩小到地址&amp;工具栏.<a href="javascript:void(0);"><img class="alignnone size-full wp-image-88" title="kuozhan-01" src="./Chrome定制安装/kuozhan-01-e1275070971523.jpg" alt="扩展" width="148" height="39"/></a></p><p>修改chrome的快捷方式,加上“ -bookmark-menu”</p><p>2、使用Greasemonkey脚本</p><p>修改chrome的快捷方式,加上“ -enable-user-scripts”</p><p>启动chrome后,会在用户数据目录下生成一个User\r\n\t\t\t\t\t\t\t\tScripts目录,将Greasemonkey脚本复制到这个目录,重启chrome即可.</p><p>我使用“ -enable-user-scripts\r\n\t\t\t\t\t\t\t\t-user-data-dir=C:\\chrome-user-data-dir” ,这样,双系统环境下,就能使用同一份用户数据了.</p><p><span style="color: #327E00; line-height: 1.8em">——————-其他参数————————–</span></p><p>–disable-plugins 禁用插件</p><p>–User-data-dir=dir 设置缓存</p><p>–single-process 单线程模式</p><p>–no-sandbox 非沙箱模式</p><p>process-per-tab 每个标签使用单独进程</p><p>process-per-site 每个站点使用单独进程</p><p>in-process-plugins 插件不启动单独进程</p><p>start-maximized 启动就最大化</p><p>first-run 第一次运行</p><p>disable-popup-blocking 禁用弹出拦截</p><p>disable-javascript 禁用javascript</p><p>disable-java 禁用java</p><p>disable-images 禁用图像</p><p>app 应用模式</p><p>kiosk 全屏模式</p></li></ul><p>&nbsp; &nbsp;\r\n\t\t\t<!-- main --></p><p>&nbsp; &nbsp;\r\n\t\t<!-- sleeve --></p><!--!doctype-->	11	1	1	2014-01-27	\N	\N	2014-01-28	2014-01-30	\N	\N	\N
5	2	科技	<title>\r\n    内核裁剪\r\n</title>\r\n<pre> 内核裁剪\r\n\t1.源码目录：/usr/src/kernels或/usr/src下\r\n\t2.内核裁剪目录介绍：http://blog.csdn.net/skyflying2012/article/details/8660677\r\n\t3.menuconfig配置：\r\n\thttp://wenku.baidu.com/view/efc745e8856a561252d36fe7.html\r\n\thttp://www.chinaunix.net/old_jh/4/1051808.html\r\n\t4.内核编译：\r\n\thttp://www.cnblogs.com/top5/archive/2011/03/04/1970645.html\r\n\t linux2.6内核编译步骤：http://blog.chinaunix.net/uid-26404477-id-3126818.html\r\n\t http://blog.csdn.net/csdn_document/article/details/416838</pre>\r\n<pre>  问题解决\r\n  1.\r\n  编译Linux2.6.31.7内核出错,找不到ncurses libraries ，该怎么办\r\n\tUnable to find the ncurses libraries or the\r\n\t *** required header files.\r\n\t *** &#39;make menuconfig&#39; requires the ncurses libraries.\r\n  解决：安转ncurses开发库 如果是debian或ubuntu系统可用命令 apt-get install libncurses5-dev\r\n  (CentOS或RedHat用yum install ncurses\\yum install ncurses-devel \r\n  参考：http://blog.sina.com.cn/s/blog_66927d490100qi5l.html)</pre><!--!doctype-->\r\n<p>\r\n    <br/>\r\n</p>	19	3	3	\N	\N	\N	\N	\N	\N	\N	\N
2	1	德润产业	<p>测试</p>	11	2	3	\N	\N	\N	\N	\N	\N	\N	home_09.gif
3	2	房地产	\N	19	3	1	\N	\N	\N	\N	\N	\N	\N	\N
13	9	企业文化	<p style="color: rgb(59, 51, 12); font-family: 宋体; font-size: 12px; line-height: 22.799999237060547px; white-space: normal; background-color: rgb(255, 255, 255);"><strong>以德载天下</strong>&nbsp;<strong>以诚润乾坤</strong></p><p style="color: rgb(59, 51, 12); font-family: 宋体; font-size: 12px; line-height: 22.799999237060547px; white-space: normal; background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp;&nbsp; 作为一家综合性企业集团始终秉持德能载物，诚可立邦的崇高信念。昨天，德润从雨雪风霜中走来；今天，德润正在脚踏实地的奋进中；相信，明天的德润必将是物华秋实，硕果累累。</p><p>◇&nbsp;&nbsp; 企业宗旨：以德立业，以信润志</p><p>◇&nbsp;&nbsp; 德润精神：团结、拼搏、突破、执着</p><p>◇&nbsp;&nbsp; 企业愿景：推崇务实的创业精神，不断提高技术与服务水平，勇敢承担社会责任，做一个受社会信赖与尊敬的企业。</p><p>◇&nbsp;&nbsp; 员工操守：先做人，后做事；欲正人，先正己。</p><p><br/></p>	19	3	4	\N	\N	\N	\N	\N	2014-01-29 10:29:35	1	\N
14	9	组织机构	<p><img src="http://localhost/test/ueditor/upload/20140129/90911390977576007.JPG" title="2012625111150726.JPG"/></p>	12	3	5	\N	\N	\N	\N	\N	2014-01-29 10:29:44	1	\N
20	16	社会焦点	\N	19	3	1	\N	\N	\N	\N	\N	2014-01-29 10:32:58	1	\N
9	1	关于德润	\N	11	2	2	\N	\N	\N	\N	\N	2014-01-29 10:05:14	1	home_08.gif
11	9	总裁致辞	<p><strong>岁月如诗，风雨如歌。</strong></p><p>&nbsp;&nbsp;&nbsp;&nbsp;回望德润企业集团走过的道路：是德润人不断自强的道路，是德润人不断创新的道路。正是自强与创新的精神和意志，让德润企业集团走上了更为宽广、更为辉煌的经济舞台。德润由一家名不见经传的小企业起步，逐步转型为以房地产、高科技为基础的综合性企业集团，并不断提升核心竞争力，向着德润人矢志不渝的梦想迈进，成为具有市场竞争力和行业影响力的集团公司。</p><p>&nbsp;</p><p><strong>雄关漫漫，长缨在手。</strong></p><p>&nbsp;&nbsp;&nbsp;&nbsp;昨日的辉煌如壮丽的风景，不断激励我们奋进的脚步。我们深知：市场竞争是一场没有终点的拉力赛。只有追求卓越，不断挑战自我的企业，才能始终保持领先。</p><p>&nbsp;</p><p>感谢您：曾经支持并见证了德润企业集团发展的朋友们，我们将共同创造更加雄丽的明天。</p><p><br/></p>	19	3	2	\N	\N	\N	\N	\N	2014-01-29 10:29:15	1	\N
12	9	经营战略	<p>&nbsp; &nbsp;&nbsp;&nbsp;<strong>核心竞争力战略：</strong>加大核心产业投资力度，整合各相关产业资源，形成主业突出、市场竞争力强的产业体系。</p><p>&nbsp;</p><p>&nbsp;&nbsp;&nbsp;&nbsp;<strong>&nbsp;综合经营战略：</strong>在强化核心业务的同时，寻找市场机遇，不断拓展新的经营领域，整合企业经营资源，丰富经营和服务手段，创造更多的利润增长点。</p><p>&nbsp;</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>人力资源开发战略：</strong>人才是最宝贵的资源，既要为企业现有人才创造施展才华的舞台，同时充分利用人才市场，广泛吸纳社会各界精英，建立优势互补的“两条人才渠道”，不断优化人才结构。</p><p>&nbsp;</p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>名牌战略：</strong>积极培育和发展名牌商品，依靠强大的品牌提升核心竞争力。</p><p><br/></p>	19	3	3	\N	\N	\N	\N	\N	2014-01-29 10:29:24	1	\N
16	1	信息中心	\N	11	2	4	\N	\N	\N	\N	\N	2014-01-29 10:31:45	1	home_10.gif
21	16	德润新闻	\N	19	3	2	\N	\N	\N	\N	\N	2014-01-29 10:33:07	1	\N
18	1	意见反馈	\N	11	2	6	\N	\N	\N	\N	\N	2014-01-29 10:32:10	1	home_13.gif
29	18	留言版	\N	19	3	1	\N	\N	\N	\N	\N	2014-01-29 10:35:01	1	\N
30	18	总裁信箱	\N	19	3	2	\N	\N	\N	\N	\N	2014-01-29 10:35:08	1	\N
17	1	人才招聘	\N	11	2	5	\N	\N	\N	\N	\N	2014-01-29 10:32:01	1	home_12.gif
6	2	酒店餐饮 运动休闲	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/><meta charset="UTF-8"/><title>Chrome定制安装</title><link rel="stylesheet" href="./Chrome定制安装/style.css" type="text/css" media="screen"/><meta name="robots" content="index,follow"/><meta name="description" content="Chrome定制安装"/><ul id="postlist" class=" list-paddingleft-2"><li><h2 class="transparent-title">Chrome定制安装</h2><p><a href="http://brilliance.blog.com/2010/05/19/chrome%E5%AE%9A%E5%88%B6%E5%AE%89%E8%A3%85/">Chrome定制安装</a><br/>\r\n\t\t\t\t\t\t\t\t\t: “<span style="line-height: 1.8em">用过Chrome的童鞋肯定会发现,Chrome安装过程用户无法插手,XP下强制安装在用户目录下,(PS.Google的东西貌似都有这个习惯…)</span></p><p><span style="line-height: 1.8em">可是这种做法让某些用户十分不爽,也使某些用户十分不便.例如我的20G硬盘,C盘分了4G,Google的东西默认的强制占用我不多的系统盘资源….让我十分不爽.</span></p><p><span style="line-height: 1.8em">尤其是它浏览网页的时候产生的大量缓存文件都放在安装目录下的”User\r\n\t\t\t\t\t\t\t\t\tData”中,稍不注意我的系统托盘就提示空间不足= =!</span></p><p><span style="line-height: 1.8em">OK,废话不多说,接下来对强制安装不爽的童鞋们,我们来定制它.</span></p><p><span style="line-height: 1.8em"><span style="font-weight: bold">1.更改安装路径.</span></span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t1)首先默认安装Chrome(例如4.0.249.89),然后进入它的安装目录.</span></p><p><span style="line-height: 1.8em"><span style="color: #33ff00; line-height: 1.8em"> Win7/Vista下,\r\n\t\t\t\t\t\t\t\t\t\tWin+R运行 %UserProfile%\\AppData\\Local\\Google\\Chrome<span style="font-weight: bold"> XP下, Win+R运行\r\n\t\t\t\t\t\t\t\t\t\t\t%UserProfile%\\Local Settings\\Application Data\\Google\\Chrome</span></span></span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t2).进入Application文件夹.将Application\\4.0.249.89\\目录下所有的文件移动到Application\\目录下</span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t3).将Application\\文件夹改名,例如改成Chrome.然后移动到你想要安装Chrome的目录下,例如我的\r\n\t\t\t\t\t\t\t\t\tD:\\Program Files\\Chrome</span></p><p><span style="line-height: 1.8em"><span style="font-weight: bold">2.更改缓存文件路径.</span></span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t1).随便进入一个你想要放置缓存文件的目录下,例如我的 D:\\Program Files\\Chrome</span></p><p><span style="line-height: 1.8em"> 2).在此目录下新建一个文件夹 ,例如\r\n\t\t\t\t\t\t\t\t\tChrome_Temp(此目录将来存放缓存文件).</span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t3).如果你以前曾经用过Chrome了,并且有一些插件,账户信息,历史记录等等,可以直接将原安装目录下\\Google\\Chrome\\User\r\n\t\t\t\t\t\t\t\t\tData文件夹中的 所有文件都移动到新建的缓存文件目录下.</span></p><p><span style="line-height: 1.8em"><span style="font-weight: bold">3.初始化Chrome,使变更生效.最重要的一步了</span></span></p><p><span style="line-height: 1.8em"> 1).进入命令提示符, <span style="color: #66ff00; line-height: 1.8em">Win+R 运行 cmd</span></span></p><p><span style="line-height: 1.8em"> 2).切换到自定义的Chrome安装路径.\r\n\t\t\t\t\t\t\t\t\t例如 <span style="color: #66ff00; line-height: 1.8em">cd\r\n\t\t\t\t\t\t\t\t\t\tD:\\Program Files\\Chrome</span></span></p><p><span style="line-height: 1.8em"> 3).运行 <span style="color: #66ff00; line-height: 1.8em">chrome.exe\r\n\t\t\t\t\t\t\t\t\t\t-first-run -user-data-dir=</span><span style="color: #AAAA00; line-height: 1.8em">D:\\Program\r\n\t\t\t\t\t\t\t\t\t\tFiles\\Chrome\\Chrome_Temp (黄色部分是你自己的缓存文件位置)</span></span></p><p><span style="line-height: 1.8em"> 弹出安装窗口,下一步,下一步…..</span></p><p><span style="line-height: 1.8em"><span style="font-weight: bold">4.创建快捷方式,以运行用户自定义的Chrome.(必需)</span></span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t1).在chrome.exe上右键单击–&gt;创建快捷方式,</span></p><p><span style="line-height: 1.8em"> 或者\r\n\t\t\t\t\t\t\t\t\t右键单击–&gt;发送到–&gt;桌面快捷方式</span></p><p><span style="line-height: 1.8em"> (或者 在任意空白地方\r\n\t\t\t\t\t\t\t\t\t右键—&gt;新建快捷方式–&gt;选择chrome.exe)</span></p><p><span style="line-height: 1.8em"> 然后这个快捷方式可以随便复制或者移动了.</span></p><p><span style="line-height: 1.8em"> 2).修改快捷方式的参数</span></p><p><span style="line-height: 1.8em"> 在快捷方式上 右键—&gt;属性\r\n\t\t\t\t\t\t\t\t\t在目标栏最后添加参数 -<span style="color: #66ff33; line-height: 1.8em">user-data-dir=</span><span style="color: #B1B100; line-height: 1.8em">D:\\Program\r\n\t\t\t\t\t\t\t\t\t\tFiles\\Chrome_temp</span></span></p><p><span style="line-height: 1.8em"> 例如我的 <span style="color: #AAAA00; line-height: 1.8em">“D:\\Program\r\n\t\t\t\t\t\t\t\t\t\tFiles\\Chrome\\chrome.exe” </span><span style="color: #66ff00; line-height: 1.8em">-user-data-dir=D:\\Program\r\n\t\t\t\t\t\t\t\t\t\tFiles\\Chrome_temp</span></span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t如此一来运行这个快捷方式,浏览网页产生的缓存文件便会保存在自定义的目录下了.</span></p><p><span style="line-height: 1.8em"><span style="font-weight: bold">5.修改默认文件的打开方式.</span>(非必需,但是不修改的话可能会发生你不想看到的结果=\r\n\t\t\t\t\t\t\t\t\t=!)</span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t1).打开”我的电脑”,按Alt,工具—&gt;文件夹选项—&gt;文件类型</span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t2).如果你曾默认安装过Chrome,会看到某些类型的文件默认是用Chrome打开的,例如HTML,HTM等等.</span></p><p><span style="line-height: 1.8em"> 找出这些文件,然后选中\r\n\t\t\t\t\t\t\t\t\t单击”高级”,在弹出的窗口中的操作框里找到”open”,选中 单击”编辑”.</span></p><p><span style="line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\t会看到”用于执行的应用程序”这一栏,将里边的链接添加 相关参数.</span></p><p><span style="line-height: 1.8em"> 例如我的 <span style="color: #B2B309; line-height: 1.8em">“D:\\Program\r\n\t\t\t\t\t\t\t\t\t\tFiles\\Chrome\\chrome.exe” -user-data-dir=Chrome_temp\r\n\t\t\t\t\t\t\t\t\t\t-bookmark-menu — “%1″</span></span></p><p><span style="font-size: 18px; line-height: 1.8em">\r\n\t\t\t\t\t\t\t\t\tPS.至于都有哪些类型….自己找找吧.就那么几个.</span></p><p><span style="line-height: 1.8em">经过了这么几步之后,你的Chrome已经自定义安装了.</span></p><p>只是这样做有一个后遗症……无法自动更新了….不过你可以手动更新,或者利用类似360那样的软件升级管理工具来进行升级.</p><p>—————————————————————————————————————————</p><p>Chrome的定制可不仅仅这么一个.通过修改快捷方式的参数,还可以实现更多的定制.</p><p>\r\n\t\t\t\t\t\t\t\t1、将标签栏缩小到地址&amp;工具栏.<a href="javascript:void(0);"><img class="alignnone size-full wp-image-88" title="kuozhan-01" src="./Chrome定制安装/kuozhan-01-e1275070971523.jpg" alt="扩展" width="148" height="39"/></a></p><p>修改chrome的快捷方式,加上“ -bookmark-menu”</p><p>2、使用Greasemonkey脚本</p><p>修改chrome的快捷方式,加上“ -enable-user-scripts”</p><p>启动chrome后,会在用户数据目录下生成一个User\r\n\t\t\t\t\t\t\t\tScripts目录,将Greasemonkey脚本复制到这个目录,重启chrome即可.</p><p>我使用“ -enable-user-scripts\r\n\t\t\t\t\t\t\t\t-user-data-dir=C:\\chrome-user-data-dir” ,这样,双系统环境下,就能使用同一份用户数据了.</p><p><span style="color: #327E00; line-height: 1.8em">——————-其他参数————————–</span></p><p>–disable-plugins 禁用插件</p><p>–User-data-dir=dir 设置缓存</p><p>–single-process 单线程模式</p><p>–no-sandbox 非沙箱模式</p><p>process-per-tab 每个标签使用单独进程</p><p>process-per-site 每个站点使用单独进程</p><p>in-process-plugins 插件不启动单独进程</p><p>start-maximized 启动就最大化</p><p>first-run 第一次运行</p><p>disable-popup-blocking 禁用弹出拦截</p><p>disable-javascript 禁用javascript</p><p>disable-java 禁用java</p><p>disable-images 禁用图像</p><p>app 应用模式</p><p>kiosk 全屏模式</p></li></ul><p>\r\n\t\t\t<!-- main --></p><p>\r\n\t\t<!-- sleeve --></p><!--!doctype-->	19	3	4	2014-01-27	\N	\N	2014-01-27	2015-01-29	\N	\N	\N
15	2	经贸 投资	\N	19	3	5	\N	\N	\N	\N	\N	2014-01-29 10:31:09	1	\N
19	1	联系我们	<p><br/></p><p><img src="http://localhost/test/ueditor/upload/20140129/54211390979386677.gif" title="lx.gif"/></p><p><strong>地址:</strong>北京市朝阳区建国门外大街永安东里甲3号德润大厦19层<br/><strong>传真:</strong>65698001<br/><strong>电话:</strong>65699999<br/><strong>邮编:</strong>100022<br/></p><p><iframe class="ueditor_baidumap" src="http://localhost/test/ueditor/dialogs/map/show.html#center=116.279381,39.944188&zoom=13&width=530&height=340&markers=116.279381,39.944188&markerStyles=l,A" frameborder="0" width="534" height="344"></iframe></p><p><br/></p>	19	2	7	\N	\N	\N	\N	\N	2014-01-29 10:32:20	1	home_14.gif
28	17	北京第六商务俱乐部有限公司	\N	19	3	7	\N	\N	\N	\N	\N	2014-01-29 10:34:35	1	\N
27	17	北京德润酒文化有限公司	\N	19	3	6	\N	\N	\N	\N	\N	2014-01-29 10:34:24	1	\N
26	17	德润万豪物业管理有限公司	\N	19	3	5	\N	\N	\N	\N	\N	2014-01-29 10:34:17	1	\N
25	17	北京德润逸生珠宝首饰有限公司	\N	19	3	4	\N	\N	\N	\N	\N	2014-01-29 10:34:05	1	\N
24	17	北京华通网信投资发展有限公司	\N	19	3	3	\N	\N	\N	\N	\N	2014-01-29 10:33:54	1	\N
23	17	美泉宫饭店	\N	19	3	2	\N	\N	\N	\N	\N	2014-01-29 10:33:45	1	\N
22	17	集团总部	\N	19	3	1	\N	\N	\N	\N	\N	2014-01-29 10:33:30	1	\N
8	1	首页	<p>首页</p>	19	2	1	\N	\N	\N	\N	\N	2014-01-29 09:47:23	1	home_07.gif
10	9	德润介绍	<p>\r\n    \t\t\t\t\r\n</p>\r\n<p>\r\n    <span style="color: rgb(59, 51, 12); font-family: 宋体; font-size: 12px; line-height: 22.799999237060547px; background-color: rgb(255, 255, 255);">&nbsp; &nbsp; 德润企业集团是一家多元化、跨地域的综合性企业集团，现有下属子公司近二十家，经营涉及房地产开发、黄金珠宝、IT产业、生物科技、物业管理、酒店餐饮、运动休闲、酒业、矿业、能源等多个领域，业务范围遍及全国。</span><br style="color: rgb(59, 51, 12); font-family: 宋体; font-size: 12px; line-height: 22.799999237060547px; white-space: normal; background-color: rgb(255, 255, 255);"/><span style="color: rgb(59, 51, 12); font-family: 宋体; font-size: 12px; line-height: 22.799999237060547px; background-color: rgb(255, 255, 255);">&nbsp;</span><br style="color: rgb(59, 51, 12); font-family: 宋体; font-size: 12px; line-height: 22.799999237060547px; white-space: normal; background-color: rgb(255, 255, 255);"/><span style="color: rgb(59, 51, 12); font-family: 宋体; font-size: 12px; line-height: 22.799999237060547px; background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp; 德润企业集团始终遵循“以德立业，以信润志”的企业宗旨，秉持“团结、拼搏、突破、执着”的企业精神，凭借优秀的管理思想、独到的经营理念、卓越的产品质量、雄厚的技术实力、细致高效的服务、务实的创业精神、诚实守信的商业信誉，博得了广大客户及众多合作伙伴的信赖与支持。</span><br style="color: rgb(59, 51, 12); font-family: 宋体; font-size: 12px; line-height: 22.799999237060547px; white-space: normal; background-color: rgb(255, 255, 255);"/><span style="color: rgb(59, 51, 12); font-family: 宋体; font-size: 12px; line-height: 22.799999237060547px; background-color: rgb(255, 255, 255);">&nbsp;</span><br style="color: rgb(59, 51, 12); font-family: 宋体; font-size: 12px; line-height: 22.799999237060547px; white-space: normal; background-color: rgb(255, 255, 255);"/><span style="color: rgb(59, 51, 12); font-family: 宋体; font-size: 12px; line-height: 22.799999237060547px; background-color: rgb(255, 255, 255);">&nbsp;&nbsp;&nbsp; 面对日益激烈的市场竞争，德润企业集团将一如既往地坚持自己的品牌理念与企业品格，不断从实践中总结经验，勇于进取，与社会各界通力合作，共谋企业发展并更好的回馈社会。</span>\r\n</p>\r\n<p>\r\n    \r\n\t\t\t\r\n</p>	19	3	1	\N	\N	\N	\N	\N	2014-01-29 10:29:04	1	\N
\.


--
-- TOC entry 2037 (class 0 OID 0)
-- Dependencies: 177
-- Name: tb_column_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test
--

SELECT pg_catalog.setval('tb_column_id_seq', 31, true);


--
-- TOC entry 1853 (class 2606 OID 71808)
-- Name: pk_dict_id; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY sys_dict
    ADD CONSTRAINT pk_dict_id PRIMARY KEY (dict_id);


--
-- TOC entry 1857 (class 2606 OID 71821)
-- Name: pk_sys_dict_detail_id; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY sys_dict_detail
    ADD CONSTRAINT pk_sys_dict_detail_id PRIMARY KEY (detail_id);


--
-- TOC entry 1859 (class 2606 OID 71835)
-- Name: pk_sys_user_userid; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY sys_user
    ADD CONSTRAINT pk_sys_user_userid PRIMARY KEY (userid);


--
-- TOC entry 1861 (class 2606 OID 71847)
-- Name: pk_tb_column_id; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY tb_column
    ADD CONSTRAINT pk_tb_column_id PRIMARY KEY (id);


--
-- TOC entry 1855 (class 2606 OID 71810)
-- Name: uk_sys_dict_type; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY sys_dict
    ADD CONSTRAINT uk_sys_dict_type UNIQUE (dict_type);


--
-- TOC entry 1862 (class 2606 OID 71822)
-- Name: fk_sys_dict_type; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY sys_dict_detail
    ADD CONSTRAINT fk_sys_dict_type FOREIGN KEY (dict_type) REFERENCES sys_dict(dict_type);


--
-- TOC entry 1985 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: Administrator
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM "Administrator";
GRANT ALL ON SCHEMA public TO "Administrator";
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2014-02-07 16:21:16

--
-- PostgreSQL database dump complete
--

