<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@include file="/static/component/include/head.jsp"%>
</head>
<body>
	<form name="form1" action="" method="post">
		<div id="tools">
			<span class="new_page">任务状态查看</span>
		</div>
		<!-- 数据列表 -->
		<div class="tableEdit" >
			<table>
				<tr>
					<td>状态编码</td>
					<td>${item.stateNum}</td>
				</tr>
				<tr>
					<td>类型</td>
					<td>${dictMap[item.stateType].detailName}</td>
				</tr>
				<tr>
					<td>状态</td>
					<td>${item.state}</td>
				</tr>
			</table>
		</div>
		<div id="bottom">
			<input type="button" class="btn1" value="关 闭" onclick="closeIframe();" />
		</div>
	</form>
</body>

</html>
