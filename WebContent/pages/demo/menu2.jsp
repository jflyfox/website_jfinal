<%@page import="com.flyfox.modules.column.TbColumn"%>
<%@page import="com.flyfox.util.StrUtils"%>
<%@page import="com.flyfox.modules.dict.DictCache"%>
<%@page import="com.flyfox.util.NumberUtils"%>
<%@page import="java.util.List"%>
<%@page import="com.flyfox.modules.column.ColumnSvc"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>XX企业集团网站</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="jquery-1.8.2.min.js"></script>
<style>
* {
	padding: 0； margin : 0;
}

#container {
	height: 700px;
	width: 1000px;
	margin: 0 auto;
}

#container ul li a,.menu ul li a:visited {
	display: block;
	color: black;
	padding: 0px 0px 0px 20px;
	text-decoration: none;
	background: url('menu2/menu_page_process.png') no-repeat;
}

#container ul li a:hover,.menu ul li a:hover.exit,.menu ul li a.home:hover
	{
	color: rgb(173, 173, 173);
	background: url('menu2/menu_next.png') no-repeat;
}

#menu1 {
	border: 1px solid black;
	height: 50px;
	width: 1000px;
}

#menu1 ul {
	list-style: none;
	padding: 0px;
	margin: 0px;
	margin-left: 100px;
	margin-top: 10px;
	margin-bottom: 10px;
}

#menu1 ul li {
	float: left;
	margin: 5px 0 5px 0;
	padding-left: 10px;
	padding-right: 10px;
	font-size: 14px;
	font-family: 微软雅黑;
	line-height: 18px;
	height: 18px;
}

#menu2 {
	float: left;
	border: 1px solid black;
	height: 650px;
	width: 300px;
}

#menu2 ul {
	list-style: none;
	padding: 0px;
	margin: 0px;
	margin-left: 5px;
	margin-top: 10px;
	margin-bottom: 10px;
}

#menu2 ul li {
	margin: 5px 0 5px 0;
	padding-left: 5px;
	padding-right: 10px;
	font-size: 14px;
	font-family: 微软雅黑;
	line-height: 18px;
	height: 18px;
}

#article {
	border: 1px solid black;
	float: left;
	width: 696px;
	min-height: 650px;
}
</style>
<%
	String para_id = request.getParameter("id");
	TbColumn article = ColumnSvc.getColumn(NumberUtils.parseInt(para_id, 8));
%>
<script type="text/javascript">
<!--
	function show1(pid) {
		$("ul[class^='daniu']").hide();
		$("ul.daniu_" + pid).show();
	}

	$(function($) {
		$("ul[class^='daniu']").hide();
		$("ul.daniu_<%=article.getInt("parent_id") %>").show();
	});
//-->
</script>
</head>

<body>
	<form action="" name="form1">
		<div id="container">

			<div id="menu1">
				<ul>
					<%
						int type,id;
						String title;
						List<TbColumn> list1 = ColumnSvc.getListByParentId(1);
						for (TbColumn column : list1) {
							type = NumberUtils.parseInt(DictCache.getCode(column.getInt("type")));
							id = column.getInt("id");
							title = column.getStr("title");
					%>
					<li>
						<% if(type==1){ %>
							<a href="javascript:show1(<%=id %>)"><%=title %></a>
						<% } else if(type==2){ %>
							<a href="menu2.jsp?id=<%=id %>"><%=title %></a>
						<% } else if(type==3){ %>
							<a href="../../article/show/<%=id %>" target="_blank "><%=title %></a>
						<%} %>
					</li>
					<%
						}
					%>
				</ul>
			</div>

			<div id="menu2">
				<%
					for (TbColumn column1 : list1) {
				%>
				<ul class="daniu_<%=column1.getInt("id")%>">
					<%
						List<TbColumn> list2 = ColumnSvc.getListByParentId(column1.getInt("id"));
							for (TbColumn column2 : list2) {
								type = NumberUtils.parseInt(DictCache.getCode(column2.getInt("type")));
								id = column2.getInt("id");
								title = column2.getStr("title");
					%>
					<li>
						<% if(type==1){ %>
							<a href="javascript:show2(<%=id %>)"><%=title %></a>
						<% } else if(type==2){ %>
							<a href="menu2.jsp?id=<%=id %>"><%=title %></a>
						<% } else if(type==3){ %>
							<a href="../../article/show/<%=id %>" target="_blank "><%=title %></a>
						<%} %>
					</li>
					<%
						}
					%>
				</ul>
				<%
					}
				%>
			</div>

			<div id="article">
				<h3 align="center" id="titile"><%=article.getStr("title")%></h2>
					<div id="content" style="padding: 0px 10px 0px 10px;">
						<%=StrUtils.nvl(article.getStr("content"),"暂无") %>
					</div>
			</div>
			
		<div>
	</form>
</body>
</html>
