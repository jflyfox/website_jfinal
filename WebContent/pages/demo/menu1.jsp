<%@page import="com.flyfox.modules.column.TbColumn"%>
<%@page import="com.flyfox.util.StrUtils"%>
<%@page import="com.flyfox.util.NumberUtils"%>
<%@page import="com.flyfox.modules.dict.DictCache"%>
<%@page import="com.flyfox.modules.column.ColumnSvc"%>
<%@page import="java.util.List"%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>XX企业集团网站</title>
<link href="./menu1/style.css" rel="stylesheet" type="text/css">
<link href="./menu1/menu_style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="jquery-1.8.2.min.js"></script>
<%
	String para_id = request.getParameter("id");
	TbColumn article = ColumnSvc.getColumn(NumberUtils.parseInt(
	para_id, 8));
%>
<script type="text/javascript">
<!--
	function show1(pid) {
		$("ul[class^='daniu']").hide();
		$("ul.daniu_" + pid).show();
	}

	$(function($) {
	});
//-->
</script>
</head>

<body>
	<div align="center">
		<table align="center" cellpadding="0" cellspacing="0"
			class="index_table"  style="background: #EEE">
			<tr>
				<td valign="top"><table cellspacing="0" cellpadding="0">
						<tr>
							<td height="28" background="./menu1/menu_bg_1.gif"><table
									align="center" cellpadding="0" cellspacing="0">
									<tr>
										<td width="50" align="center">&nbsp;</td>
										<td height="24" align="center" nowrap=""><div
												class="menu" style="border: 0px solid #000000">
												<ul>
						<%
							List<TbColumn> list1 = ColumnSvc.getListByParentId(1);
							int type,id;
							String title;
							for (TbColumn column1 : list1) {
								type = NumberUtils.parseInt(DictCache.getCode(column1.getInt("type")));
								id = column1.getInt("id");
								title = column1.getStr("title");
						%>
						<li>
							<% if(type==1){ %>
								<a href="javascript:show2(<%=id %>)">
							<% } else if(type==2){ %>
								<a href="menu1.jsp?id=<%=id %>">
							<% } else if(type==3){ %>
								<a href="../../article/show/<%=id %>" target="_blank ">
							<%} %><strong><%=column1.getStr("title") %></strong></a>
						<ul>
							<%
								List<TbColumn> list2 = ColumnSvc.getListByParentId(column1.getInt("id"));
									for (TbColumn column2 : list2) {
										type = NumberUtils.parseInt(DictCache.getCode(column2.getInt("type")));
										id = column2.getInt("id");
										title = column2.getStr("title");
							%>
							<li>
								<% if(type==1){ %>
									<a href="javascript:show2(<%=id %>)"><%=title %></a>
								<% } else if(type==2){ %>
									<a href="menu1.jsp?id=<%=id %>"><%=title %></a>
								<% } else if(type==3){ %>
									<a href="../../article/show/<%=id %>" target="_blank "><%=title %></a>
								<%} %>
							</li>
							<%
								}
							%>
						</ul>
						</li>
						<li><img src="./menu1/little_1.gif"></li>
						<%
							}
						%>
												</ul>
											</div>
										</td>
										<td width="50" align="center">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>

			</tr>
			<tr>
				<td style="margin-top: 20px;">
				<div style="margin-left: 40px;">
				<%=StrUtils.nvl(article.getStr("content"),"暂无") %>
				</div>
				</td>
			</tr>
		</table>

	</div>
</body>
</html>